# Campaign UI
Converting a design (picture) into a responsive design with preset data as needed!

## Live
https://campaign-vert.vercel.app/

## How did I start this project?
I start the project by separating it to small chunk's and then add it to my Pomodoro app.


* [1-Design](#mobile-design)


   Why design first?

> Think twice, code once!


## Mobile Design
So the goal here is to make a mobile first approach concept.
I designed 2 version for the mobile screen sizes:

This is all in one page, which is not good option!
![mobile v1](design/mobile-v1.jpg)

This is an improved version that is far superior!

Campaign Details            |  Active Campaign
:-------------------------:|:-------------------------:
![mobile v2 Campaign Details](design/mobile–v2-campaign-details.jpg) | ![mobile v2 Active Campaign](design/mobile–v2-active-campaign.jpg)

**Why did I put the labels above the input rather than next to it?**

According to the Matteo Penzo’s [eye-tracking research](https://www.uxmatters.com/mt/archives/2006/07/label-placement-in-forms.php):

![https://www.uxmatters.com/mt/archives/2006/07/images/test3.jpg](https://www.uxmatters.com/mt/archives/2006/07/images/test3.jpg)

> Placing a label right over its input field permitted users to capture both elements with a single eye movement.

Why I didnt use the float labels?

While float labels might seem good but unfortunately, there are several problems with this approach:

- First, there is no space for a hint because the label and hint are one and the same. Second, they’re hard to read, due to their poor contrast and small text, as they’re typically designed. Third, like placeholders, they may be mistaken for a value and could get cropped.

> “Seems like a lot of effort when you could simply put labels above inputs & get all the benefits/none of the issues.”
>
- Quirky and minimalist interfaces don’t make users feel awesome — obvious, inclusive, and robust interfaces do!

<br/>

**Why 2 page?**

As a result, rather than doing it as an SPA, I chose to develop a separate page (route).
why?

<br/>

**One thing per page:**

it is about splitting up a complicated process into small chunks and placing them on screens of their own.

Inclusive design principle 6 says:

> we should design interfaces that help users focus on core tasks by prioritizing them
>

<br/>

It even goes on to say that people should be able to focus on one thing at a time

- One thing per page simply follows this principle to the letter, and in doing so drastically reduces the cognitive burden on users.
- When users fill in a small form, errors are caught and shown early and often. If there’s one thing to fix, it’s easy to fix, which reduces the chance of users giving up on the task.
- If pages have little on them, they’ll load quickly. Faster pages reduce the risk of users leaving, and they build trust.
- By submitting information frequently, we can save user information in a more granular fashion. If a user drops out, we can, for example, send them an email prompting them to complete their order.
- It adds a sense of progression and increases momentum because the user is constantly moving forwards step by step.

<br/>

**Flow and Order**

In Forms That Work, Caroline Jarrett and Gerry Gaffney explain the importance of asking questions in a sensible order:

> “Asking for information at the wrong time can alienate a user. The same question put at the right moment can be entirely acceptable. Think about buying a car. You’re just browsing, getting a sense of what is available. A salesperson comes along and starts to ask you how you’ll pay. Would you answer? Or would you think, ‘If that person doesn’t stop annoying me, I’m out of here’? Now think about the point where you’ve told the salesperson which car you want to buy. Now it’s appropriate to start negotiating about payment. It would be quite odd if the salesperson did NOT do so.”
>



## Run locally
### Compiles and hot-reloads for development
```
pnpm dev
```

### Compiles and minifies for production
```
pnpm build
```


## License

[MIT](https://choosealicense.com/licenses/mit/)
