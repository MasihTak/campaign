import { createRouter, createWebHistory } from 'vue-router'
import Home from "@/views/Home.vue";
import ActiveView from "@/views/ActiveView.vue";

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
    },
    {
        path: '/activecampaign',
        name: 'activeView',
        component: ActiveView,
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router
